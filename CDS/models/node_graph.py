class NodeGraph:
    def __init__(self, id, text, pos, tag, lemma, head_text, head_pos, head_tag, head_lemma, dep, is_stop, parent_id):
        self.id = id
        self.text = text
        self.pos = pos
        self.tag = tag
        self.lemma = lemma
        self.head_text = head_text
        self.head_pos = head_pos
        self.head_tag = head_tag
        self.head_lemma = head_lemma
        self.dep = dep
        self.is_stop = is_stop
        self.parent_id = parent_id