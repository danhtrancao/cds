import en_core_web_lg
import json
import pandas
import os
# import glob
from nltk.tokenize import RegexpTokenizer
from param_parser import parameter_parser
from models.node_graph import NodeGraph
from models.node_feature import NodeFeature
from sklearn import svm
from datetime import datetime

nlp = en_core_web_lg.load()


def convert_document(sentence):
    return nlp(sentence)


def remove_punctuation(sentence):
    tokenizer = RegexpTokenizer(r'\w+')
    remove_punctuation_sentences = tokenizer.tokenize(sentence)
    remove_punctuation_sentence = ' '.join(map(str, remove_punctuation_sentences))
    return convert_document(remove_punctuation_sentence)


def build_graph(args, json_dict, file_index):
    # input_premise
    sentence1 = json_dict['sentence1']
    document_premise = remove_punctuation(sentence1)

    # input_hypothesis
    sentence2 = json_dict['sentence2']
    document_hypothesis = remove_punctuation(sentence2)

    # build graph 1
    graph_index_premise, index_largest = create_graph_premise(document_premise, args)
    graph_premise = convert_to_graph(graph_index_premise)

    # build graph 2
    graph_index_hypothesis = create_graph_hypothesis(document_hypothesis, index_largest, args)
    graph_hypothesis = convert_to_graph(graph_index_hypothesis)

    # merge 2 graphs into 1 graph
    graph_merge = merge_graph(graph_premise, graph_hypothesis)

    # remove stop word in graph
    graph_remove_stop_word = remove_stop_word_graph(graph_merge)

    # create node features
    node_features = count_node_dimension(graph_remove_stop_word)

    # print to test
    # print_test(document_premise,
    #            document_hypothesis,
    #            graph_premise,
    #            graph_hypothesis,
    #            graph_merge,
    #            graph_remove_stop_word,
    #            node_features)

    # write json file follow format graph to vector
    # write_json_file(graph_remove_stop_word, node_features, args.output_train_path, file_index)
    write_json_file(graph_remove_stop_word, node_features, args.output_test_path, file_index)


def create_graph_premise(document_premise, args):
    graph = []
    graph_premise_index = 0
    index_largest = 0

    for token in document_premise:
        parent_id = args.id_root_node
        # find the largest index to set the first index for text 2 for purposes of merge 2 graphs
        if token.dep_ != args.root_dep:
            parent_id = args.id_node
        node = NodeGraph(graph_premise_index,
                         token.text,
                         token.pos_,
                         token.tag_,
                         token.lemma_,
                         token.head.text,
                         token.head.pos_,
                         token.head.tag_,
                         token.head.lemma_,
                         token.dep_,
                         token.is_stop,
                         parent_id)
        graph.append(node)
        graph_premise_index += 1
        index_largest = graph_premise_index

    return graph, index_largest


def create_graph_hypothesis(document_hypothesis, index_largest, args):
    graph = []
    node_index = index_largest

    for token in document_hypothesis:
        parent_id = args.id_root_node
        if token.dep_ != args.root_dep:
            parent_id = args.id_node
        node = NodeGraph(node_index,
                         token.text,
                         token.pos_,
                         token.tag_,
                         token.lemma_,
                         token.head.text,
                         token.head.pos_,
                         token.head.tag_,
                         token.head.lemma_,
                         token.dep_,
                         token.is_stop,
                         parent_id)
        graph.append(node)
        node_index += 1

    return graph


def convert_to_graph(graph):
    for node_premise in graph:
        for node_hypothesis in graph:
            if node_premise.head_text == node_hypothesis.text \
                    and node_premise.head_pos == node_hypothesis.pos \
                    and node_premise.head_tag == node_premise.head_tag \
                    and node_premise.head_lemma == node_premise.head_lemma:
                node_premise.parent_id = node_hypothesis.id
    return graph


def merge_graph(graph_premise, graph_hypothesis):
    for premise in graph_premise:
        for hypothesis in graph_hypothesis:
            if premise.lemma == hypothesis.lemma:
                # get id of hypothesis
                # from id of hypothesis retrieve parent_id of other nodes
                hypothesis.id = premise.id
                hypothesis_parent_ids = [hypo for hypo in graph_hypothesis if hypo.parent_id == hypothesis.id]
                # replace those parent_id by id of premise
                for hypothesis_parent_id in hypothesis_parent_ids:
                    hypothesis_parent_id.parent_id = premise.id
                # delete node in graph_hypothesis
                # graph_hypothesis.remove(hypothesis)
    return graph_premise + graph_hypothesis


def count_node_dimension(graph):
    node_features = []

    for node in graph:
        if node.id != node.parent_id:
            # count node dimension in node id
            feature = next((x for x in node_features if x.node == node.id), None)
            if feature is None:
                node_feature = NodeFeature(node.id, 1)
                node_features.append(node_feature)
            else:
                feature.dimension += 1

            # count node dimension in parent node id
            feature_in_parent = next((x for x in node_features if x.node == node.parent_id), None)
            if feature_in_parent is None:
                node_feature = NodeFeature(node.parent_id, 1)
                node_features.append(node_feature)
            else:
                feature_in_parent.dimension += 1

    return node_features


def remove_stop_word_graph(graph_merge):
    graph_remove = []
    for node in graph_merge:
        if node.is_stop:
            # append those node is stop word
            graph_remove.append(node)
            # find list stop word from node's id
            stop_word_parent_ids = [stop for stop in graph_merge if stop.parent_id == node.id]
            for stop_word_parent_id in stop_word_parent_ids:
                # # from stop_word_parent_id find those related id and append it
                # stop_word_related_parent_ids = [stop for stop in graph_merge if
                #                                 stop.parent_id == stop_word_parent_id.id]
                # for stop_word_related_parent_id in stop_word_related_parent_ids:
                #     # append those node is stop word
                #     graph_remove.append(stop_word_related_parent_id)
                # append those node is stop word
                graph_remove.append(stop_word_parent_id)

    return list(set(graph_merge) - set(graph_remove))


def write_json_file(graph, node_features, output_path, file_name_index):
    data = {'edges': [], 'features': {}}

    # create edges array
    for node in graph:
        if node.id != node.parent_id:
            data['edges'].append([node.id, node.parent_id])

    # create node features dictionary
    for node in node_features:
        data['features'][f'{node.node}'] = f'{node.dimension}'

    # create folder to contain json file (case 1)
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    # get last file name json to create a new json file  (case 1)
    # index = 0
    # list_of_files = glob.glob(f"{output_path}*.json")
    # if list_of_files:
    #     latest_file = max(list_of_files, key=os.path.getctime)
    #     base = os.path.basename(latest_file)
    #     last_file_name = os.path.splitext(base)[0]
    #     index = int(last_file_name) + 1
    # file_name = f'{index}.json'

    # get hard index for improve performance (case 2)
    file_name = f'{file_name_index}.json'

    with open(f'{output_path}{file_name}', 'w') as outfile:
        json.dump(data, outfile)


def print_test(document_premise,
               document_hypothesis,
               graph_premise,
               graph_hypothesis,
               graph_merge,
               graph_remove_stop_word,
               node_features):
    # dependency parsing document_premise
    for token in document_premise:
        print('doc1 dep:',
              token.text,
              token.pos_,
              token.tag_,
              token.is_stop,
              token.head.text,
              token.head.pos_,
              token.head.tag_,
              token.head.lemma_,
              token.dep_,
              [child for child in token.children])

    # dependency parsing document_hypothesis
    print('-----------------------------')
    for token in document_hypothesis:
        print('doc2 dep:',
              token.text,
              token.pos_,
              token.tag_,
              token.is_stop,
              token.head.text,
              token.head.pos_,
              token.head.tag_,
              token.dep_)

    print('-----------------------------')
    print('edges 1:')
    for node in graph_premise:
        print(node.text, node.id, node.parent_id)

    print('-----------------------------')
    print('edges 2:')
    for node in graph_hypothesis:
        print(node.text, node.id, node.parent_id)

    print('-----------------------------')
    print('edges after merged:')
    for node in graph_merge:
        print(node.text, node.id, node.parent_id)

    print('-----------------------------')
    print('edges after removed:')
    for node in graph_remove_stop_word:
        print(node.text, node.id, node.parent_id)

    print('-----------------------------')
    print('node features:')
    for node in node_features:
        print(node.node, node.dimension)


def classification():
    # read file data train
    df = pandas.read_csv('D:\\Thesis\\test_data\\features\\train\\vector_07012021105817.csv')
    # loop file graph2vec and init index to get gold_label from data train
    json_lines = read_files(args.train_data_path)

    x = []
    y = []

    i = 0
    is_init = True

    for d in df.values:
        print(f'bat dau voi {i}: ')
        json_dict = json.loads(json_lines.__getitem__(i))
        print(i, json_dict['sentence1'], json_dict['sentence2'], json_dict['gold_label'])

        # set label in text
        if json_dict['gold_label'] == 'contradiction':
            yy = 1
        else:
            yy = 0
        y.append(yy)
        cc = []
        for dd in d:
            if is_init:
                is_init = False
                continue
            cc.append(dd)
        x.append(cc)
        is_init = True
        i += 1

    clf = svm.SVC()
    clf.fit(x, y)

    # read file data test
    df1 = pandas.read_csv('D:\\Thesis\\test_data\\features\\test\\vector_07012021130243.csv')
    z = []
    k = 0
    is_init1 = True

    # test file
    for d1 in df1.values:
        print(f'bat dau voi {k}: ')
        cc = []
        for dd1 in d1:
            if is_init1:
                is_init1 = False
                continue
            cc.append(dd1)
        z.append(cc)
        is_init1 = True
        k += 1

    print('predict nà:')
    dtc_predict = clf.predict(z)
    for dtc in dtc_predict:
        f = open('D:/Thesis/result/dtc_output_1.txt', "a")
        f.write(f'{dtc}, ')
    print('done')


def measure_performance():
    # read result file
    file_data = open('D:\\Thesis\\result\\dtc_output_old.txt', "r")
    data = file_data.readlines()
    data_splits = data[0].split(',')
    # read test data
    lines = read_files('D:\\Thesis\\test_data\\dtc_test_data_10012021230824.txt')
    index = 0
    count = 0
    # compare data
    for item in data_splits:
        if index == len(data_splits) - 1:
            break

        line = lines.__getitem__(index)
        json_dict = json.loads(line)
        label = json_dict['gold_label']

        if label == 'contradiction':
            label = '1'
        else:
            label = '0'

        if item.strip() == label:
            count += 1

        index += 1
    percentage = (count/len(data_splits)) * 100
    print(f'{percentage} %')


# read file from path
def read_files(path):
    file_data = open(path, "r")
    data = file_data.readlines()
    return data


def filter_data_train(args):
    # lines = read_files(args.raw_train_data_path)
    lines = read_files(args.raw_test_data_path)
    is_init = True
    filter_index = 0

    # set name for train data
    now = datetime.now()
    date_time = now.strftime("%d%m%Y%H%M%S")
    # file = open(f'D:\\Thesis\\test_data\\dtc_train_data_{date_time}.txt', 'a')
    file = open(f'D:\\Thesis\\test_data\\dtc_test_data_{date_time}.txt', 'a')

    for line in lines:
        # get index of sentence 1 to avoid string format not correct to convert
        index_gold_label = line.index('"gold_label"')

        # take line off length with last index got
        line_gold_label = line[index_gold_label:]
        json_filter_data_dict = json.loads('{' + line_gold_label)

        # filter json remove entailment
        # if json_filter_data_dict['gold_label'] == 'entailment':
        #     continue
        if not is_init:
            file.write('\n')

        # create line in json text
        sentence_1 = str(json_filter_data_dict["sentence1"]) \
            .replace('.', '') \
            .replace('"', "'") \
            .replace('\\', '') \
            .replace('- ', '-')

        sentence_2 = str(json_filter_data_dict["sentence2"]) \
            .replace('.', '') \
            .replace('"', "'") \
            .replace('\\', '') \
            .replace('- ', '-')

        gold_label = str(json_filter_data_dict["gold_label"])
        json_combine_text = '{' + f'"sentence1": "{sentence_1}.", ' \
                                  f'"sentence2": "{sentence_2}.", ' \
                                  f'"gold_label": "{gold_label}"' + '}'
        file.write(json_combine_text)
        is_init = False
        filter_index += 1
        print(filter_index, sentence_1, sentence_2, gold_label)
    file.close()
    print('dtc filter DONE!')


def build_sentence_to_graph_json(args):
    index = 0
    # json_lines = read_files(args.train_data_path)
    json_lines = read_files(args.test_data_path)
    for json_line in json_lines:
        json_dict = json.loads(json_line)
        build_graph(args, json_dict, index)
        print(index, json_dict['sentence1'], json_dict['sentence2'], json_dict['gold_label'])
        index += 1
    print('dtc build GRAPH DONE!')


if __name__ == '__main__':
    args = parameter_parser()
    # filter_data_train(args)
    # build_sentence_to_graph_json(args)
    # classification()
    measure_performance()
