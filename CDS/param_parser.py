"""Parameter parser to set the model hyperparameters."""

import argparse
from datetime import datetime


def parameter_parser():
    parser = argparse.ArgumentParser(description="Create graph from 2 texts.")

    parser.add_argument("--input-premise", nargs="?", default="An older and younger man smiling."
                        , help="Input premise text.")

    parser.add_argument("--input-hypothesis", nargs="?"
                        , default="Two men are smiling and laughing at the cats playing on the floor."
                        , help="Input hypothesis text.")

    parser.add_argument("--id-root-node", nargs="?", default=9999, help="Id of root node.")

    parser.add_argument("--id-node", nargs="?", default=10000, help="Id of node.")

    parser.add_argument("--root-dep", nargs="?", default='ROOT', help="Id of node.")

    # create name follow date time
    now = datetime.now()
    date_time = now.strftime("%d%m%Y%H%M%S")

    # create file json contains edge and node features
    parser.add_argument("--output-train-path", nargs="?", default=f"D:/Thesis/test_data/graph/dataset_train_{date_time}/"
                        , help="The path of json file.")

    parser.add_argument("--output-test-path", nargs="?", default=f"D:/Thesis/test_data/graph/dataset_test_{date_time}/"
                        , help="The path of json file.")

    parser.add_argument("--train-data-path", nargs="?", default="D:/Thesis/test_data/dtc_train_data_06012021085558.txt"
                        , help="The path of data train file. DONT'T UPDATE")

    parser.add_argument("--test-data-path", nargs="?", default="D:/Thesis/test_data/dtc_test_data_06012021090324.txt"
                        , help="The path of data train file. DONT'T UPDATE")

    parser.add_argument("--raw-train-data-path", nargs="?", default="D:/Thesis/test_data/snli_1.0_train.jsonl"
                        , help="The path of raw data train file. DONT'T UPDATE")

    parser.add_argument("--raw-test-data-path", nargs="?", default="D:/Thesis/test_data/snli_1.0_test.jsonl"
                        , help="The path of raw data train file. DONT'T UPDATE")

    parser.add_argument("--file-name", nargs="?", default=f"data_{date_time}.json"
                        , help="The file name of json file.")

    return parser.parse_args()
